import React, { Component } from 'react';

import axios from 'axios';
import react_logo from './react_logo.svg';
import node_logo from './node_logo.svg';
import './App.css';

import ReactTable from "react-table";
import "react-table/react-table.css";
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';

class DropdownSelect extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selected: props.currentColumn
    }
    this._onSelect = (option) => {
      this.setState({ selected: option.value })
      this.props.onSelect(option.value)
    }
  }

  render() {
    return (
      <Dropdown options={this.props.selectColumns} onChange={this._onSelect} value={this.state.selected} placeholder="Select an option" />
    )
  }
}

class Results extends Component {
  constructor(props) {
    super(props)
    this.state = {
      columns : [{
        Header: props => <span>{this.props.currentColumn}</span>,
        accessor: 'id'
      }, {
        Header: 'Count',
        accessor: 'count'
      }, {
        Header: 'Average age',
        accessor: 'average'
      }]
    }
  }
  render() {
    return (
      <div>
        <p> Total number of results : {this.props.count}. {this.props.count > 100 && <span>Showing first 100 lines.</span>}</p>
        <ReactTable
          data={this.props.data}
          columns={this.state.columns}
          defaultPageSize={10}
          className="-striped -highlight"
        />
      </div>
    );
  }
}

class App extends Component {
  state = {
    columns: [],
    currentColumn: '',
    currentData: [],
    count: 0
  };

  loadData = (name) => {
    axios.get('api/data/' + name)
      .then(resp => {
        this.setState({ currentData: resp.data.values, count: resp.data.count, currentColumn: name });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  componentDidMount() {
    axios.get('api/columns')
      .then(resp => {
        this.setState({ columns: resp.data.columns });
        this.loadData(resp.data.columns[0])
      })
      .catch(function (error) {
        console.log(error.response);
      });
  }
  render() {

    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Dataiku census US</h1>
          <p>Single page application to get statistics over database file.</p>
          <p>Built on React and Node.js</p>
          <img src={react_logo} className="App-logo App-logo-react" alt="logo" />
          <img src={node_logo} className="App-logo" alt="logo" />
        </header>
        <div className="App-intro">
          <div className="box">Select a column</div>
          <div className="box"><DropdownSelect selectColumns={this.state.columns} onSelect={this.loadData} currentColumn={this.state.currentColumn} /></div>
        </div>
        <div className="table">
          <Results data={this.state.currentData} count={this.state.count} currentColumn={this.state.currentColumn} />
        </div>
      </div>
    );
  }
}

export default App;
