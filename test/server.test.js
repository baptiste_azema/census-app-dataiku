const chai = require('chai');
const chaiHttp = require('chai-http');

const should = chai.should();

chai.use(chaiHttp);
const server = require('../server/server');

process.env.NODE_ENV = 'test';

describe('Status and base api', () => {
  it('should have status 404 root / GET', done => {
    chai.request(server)
      .get('/')
      .end((err, res) => {
        res.should.have.status(404);
        done();
      });
  });

  it('should respond on base api /api GET', done => {
    chai.request(server)
      .get('/api')
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.json;
        res.body.should.deep.equal({message: 'Base API'});
        done();
      });
  });
});

describe('Database and data computation', () => {
  it('should respond columns of db file /api/columns GET', done => {
    chai.request(server)
      .get('/api/columns')
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.json;
        res.body.should.have.property('columns');
        res.body.columns.should.be.a('array');
        res.body.columns[0].should.equal('age');
        done();
      });
  });

  it('should respond data statistics of "sex" column', done => {
    chai.request(server)
      .get('/api/data/sex')
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.json;

        res.body.should.have.property('values');
        res.body.values.should.be.a('array');

        res.body.values[0].should.have.property('id');
        res.body.values[0].should.have.property('count');
        res.body.values[0].should.have.property('average');
        res.body.values[0].id.should.equal('Female');

        res.body.should.have.property('count');
        res.body.count.should.be.a('number');
        done();
      });
  });
});
