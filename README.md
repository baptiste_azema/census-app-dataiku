Single-page application which allow you to get statistics of a sqlite file. Built on Node.js and React.

## Features

* Visualize basic statistics for each column : count of different values, count of each occurence, compute average age. Limit results to 100.
* Easily configurable for a different dataset using a configuration file


## Getting Started

```sh
git clone git@bitbucket.org:baptiste_azema/census-app-dataiku.git
npm install
npm run dev
```

## Build for production

Ready for production

```sh
npm run build
npm start
```

Make sure you have environment variable NODE_ENV = production

## Deploy your own instance on Now

An account on zeit.co is required. Maximum source size = 5MB

```
npm install -g now
now -e NODE_ENV=production
```

Exemple of instance on [now.sh](https://census-app-dataiku-nssqcdwzec.now.sh/). This instance only uses a sample of census data.