'use strict';
const path = require('path');
const express = require('express');
const dbUtils = require('./dbUtils');

const app = express();
if (process.env.NODE_ENV === 'production') {
  // Serve any static files
  app.use(express.static(path.join(__dirname, '../build')));

  // Handle React routing, return all requests to React app
  app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '../build', 'index.html'));
  });
} else {
  app.use(express.static(path.join(__dirname, '/')));
}

// Allows you to set port in the project properties.
app.set('port', process.env.PORT || 4000);

// ROUTES FOR OUR API
// =============================================================================
const router = express.Router();
router.get('/', (req, res) => {
  res.json({message: 'Base API'});
});

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

/*
 * GET /api/columns route to retrieve all the column names
 */
router.get('/columns', (req, res) => {
  dbUtils.getColumns(res);
});

/*
 * GET /api/data/:id route to retrieve statistics over one column represented by 'id'
 */
router.get('/data/:id', (req, res) => {
  dbUtils.getStatistics(res, req.params.id);
});

// Run express
const server = app.listen(app.get('port'), () => {
  console.log('process.env.NODE_ENV = ' + process.env.NODE_ENV);
  console.log('Express listening on port = ' + app.get('port'));
  dbUtils.connect(); // Init database
});

module.exports = server;
